﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Inventorymanagement.Web.Startup))]
namespace Inventorymanagement.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
